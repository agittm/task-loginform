﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class PlayerDataModel
{
    // Basic data
    public int Level;
    public double Exp;
    public string Username;
    public Dictionary<InGameCurrency, float> Currencies;
    public Dictionary<string, object> Achievements;

    // Tutorial data
    public List<string> TutorialFinishedList;

    public PlayerDataModel()
    {
        Level = 0;
        Exp = 0;
        Username = "Tester";
        Achievements = new Dictionary<string, object>();
        
        Currencies = new Dictionary<InGameCurrency, float>();
        Currencies.Add(InGameCurrency.Coin, 0);
        Currencies.Add(InGameCurrency.Gem, 0);

        TutorialFinishedList = new List<string>();
    }

    public PlayerDataModel(JSONNode json)
    {
        Level = json["level"].AsInt;
        Exp = json["exp"].AsDouble;
        Username = json["username"];

        Currencies = new Dictionary<InGameCurrency, float>();
        foreach(KeyValuePair<string,JSONNode> pair in json["currency"].AsObject)
        {
            Currencies.Add((InGameCurrency)System.Enum.Parse(typeof(InGameCurrency), pair.Key), pair.Value.AsFloat);
        }

        Achievements = new Dictionary<string, object>();
        foreach (KeyValuePair<string, object> pair in json["achievement"].AsObject)
        {
            Achievements.Add(pair.Key, pair.Value);
        }

        TutorialFinishedList = new List<string>();
        for (int i = 0; i < json["tutorial"].Count; i++)
        {
            TutorialFinishedList.Add(json["tutorial"][i]);
        }
    }

    public JSONObject ToJSON()
    {
        JSONObject json = new JSONObject();

        json["level"].AsInt = Level;
        json["exp"].AsDouble = Exp;
        json["username"] = Username;
        
        foreach(KeyValuePair<InGameCurrency,float> pair in Currencies)
        {
            json["currency"][pair.Key.ToString()].AsFloat = pair.Value;
        }

        foreach (KeyValuePair<string, object> pair in Achievements)
        {
            json[pair.Key] = pair.Value.ToString();
        }

        JSONArray array = new JSONArray();
        for (int i = 0; i < TutorialFinishedList.Count; i++)
        {
            array.Add(TutorialFinishedList[i]);
        }
        json["tutorial"] = array;

        return json;
    }
}