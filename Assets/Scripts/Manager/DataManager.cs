﻿using UnityEngine;
using System.IO;
using SimpleJSON;

public class DataManager : Manager
{
    public enum Locale { EN = 0, ID = 1 };
    private static string playerFileName = "/player_data.bin";
    private static string deviceFileName = "/device_data.bin";

    public static bool isEncrypted = false;
    public static Locale locale = Locale.ID;
    public static PlayerDataModel playerData;
    public static DeviceDataModel deviceData;

    protected override void OnAwake()
    {
        base.OnAwake();
    }

    public void LoadData()
    {
        if (File.Exists(Application.persistentDataPath + playerFileName))
        {
            /* === Read raw string from external file === */
            StreamReader reader = new StreamReader(Application.persistentDataPath + playerFileName);
            string saveDataString = reader.ReadToEnd();
            reader.Close();

            if (isEncrypted)
            {
                saveDataString = DataEncryptor.DecryptData(saveDataString);
            }
            /* === === */

            /* === Parse string to json object === */
            JSONNode saveData = JSONNode.Parse(saveDataString);
            /* === === */

            /* === Extract game data from json object === */
            playerData = new PlayerDataModel(saveData);
            /* === === */
        }
        else
        {
            FileStream file = File.Create(Application.persistentDataPath + playerFileName);
            file.Close();

            playerData = new PlayerDataModel();
            SavePlayerData();
        }

        if (File.Exists(Application.persistentDataPath + deviceFileName))
        {
            /* === Read raw string from external file === */
            StreamReader reader = new StreamReader(Application.persistentDataPath + deviceFileName);
            string saveDataString = reader.ReadToEnd();
            reader.Close();

            if (isEncrypted)
            {
                saveDataString = DataEncryptor.DecryptData(saveDataString);
            }
            /* === === */

            /* === Parse string to json object === */
            JSONNode saveData = JSONNode.Parse(saveDataString);
            /* === === */

            /* === Extract game data from json object === */
            deviceData = new DeviceDataModel(saveData);
            /* === === */
        }
        else
        {
            FileStream file = File.Create(Application.persistentDataPath + deviceFileName);
            file.Close();

            deviceData = new DeviceDataModel();
            SaveDeviceData();
        }
    }

    public void SavePlayerData()
    {
        /* === Create save data structure === */
        string saveData = playerData.ToJSON().ToString();

        if (isEncrypted)
        {
            saveData = DataEncryptor.EncryptData(saveData);
        }
        /* === === */

        /* === Write data to external file === */
        StreamWriter writer = new StreamWriter(Application.persistentDataPath + playerFileName);
        writer.Write(saveData);
        writer.Flush();
        writer.Close();
        /* === === */
    }

    public void SaveDeviceData()
    {
        /* === Create save data structure === */
        string saveData = deviceData.ToJSON().ToString();

        if (isEncrypted)
        {
            saveData = DataEncryptor.EncryptData(saveData);
        }
        /* === === */

        /* === Write data to external file === */
        StreamWriter writer = new StreamWriter(Application.persistentDataPath + deviceFileName);
        writer.Write(saveData);
        writer.Flush();
        writer.Close();
        /* === === */
    }

    #region Static Utility

    public static DataManager Instance
    {
        get
        {
            return Get<DataManager>();
        }
    }

    #endregion
}