﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class TutorialManager : Manager
{
    [Header("Component")]
    [SerializeField] GameObject parentTutorial;
    [SerializeField] GameObject parentMonolog;
    [SerializeField] TextMeshProUGUI textMonolog;
    [SerializeField] Transform parentCloneUI;
    [SerializeField] Transform parentCloneNonUI;

    [Header("Setting")]
    [SerializeField] bool useTyping;
    [SerializeField] Camera tutorialCamera;

    [Header("Data")]
    [SerializeField] TextAsset tutorialData;
    private List<TutorialData> tutorialList;
    private List<string> FinishedTutorialList;

    private TutorialData currentTutorial;
    private TutorialStep currentStep;
    private bool isTutorialRunning;
    private bool isReadyForInput;
    private bool isReadyForNextStep;
    private bool isActionFinished;
    private bool isTypingFinished;

    private void OnEnable()
    {
        TutorialObject.OnClicked += HandleOnTutorialObjectClicked;
    }

    private void OnDisable()
    {
        TutorialObject.OnClicked -= HandleOnTutorialObjectClicked;
    }

    protected override void OnAwake()
    {
        base.OnAwake();

        parentTutorial.SetActive(false);
        tutorialCamera.gameObject.SetActive(false);

        InitData();
    }

    private void Update()
    {
        if (isTutorialRunning && !isReadyForInput)
        {
#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0))
            {
                isReadyForInput = true;
            }
#else
            if(Input.touchCount>0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                isReadyForInput = true;
            }
#endif
        }

        if (isTutorialRunning && !isReadyForNextStep)
        {
#if UNITY_EDITOR
            if (Input.GetMouseButtonUp(0))
            {
                if (isTypingFinished && isReadyForInput)
                {
                    Next();
                }
                else
                {
                    isTypingFinished = true;
                }

                isReadyForInput = false;
            }
#else
            if(Input.touchCount>0 && Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                if (isTypingFinished && isReadyForInput)
                {
                    Next();
                }
                else
                {
                    isTypingFinished = true;
                }

                isReadyForInput = false;
            }
#endif
        }
    }

    private void InitData()
    {
        if (tutorialData == null)
            return;

        tutorialList = new List<TutorialData>();
        JSONNode json = JSON.Parse(tutorialData.text);

        for (int i = 0; i < json.Count; i++)
        {
            tutorialList.Add(new TutorialData(json[i]));
        }

        LoadData(AppManager.PlayerData.TutorialFinishedList);
    }

    public void LoadData(List<string> finishedList)
    {
        FinishedTutorialList = new List<string>(finishedList);
    }

    public void SaveData()
    {
        AppManager.PlayerData.TutorialFinishedList = FinishedTutorialList;
    }

    public bool StartTutorial(string code)
    {
        if(!tutorialList.Exists(x=>x.Id == code))
        {
            Logger.LogErrorFormat("Tutorial with id ({0}) not found.", code);
            return false;
        }
        if (FinishedTutorialList.Contains(code))
        {
            Logger.LogFormat("Tutorial with id ({0}) finished.", code);
            return false;
        }

        TutorialData tutorial = tutorialList.Find(x => x.Id == code);
        if (tutorial.Requirements.Count > 0)
        {
            for (int i = 0; i < tutorial.Requirements.Count; i++)
            {
                if (!FinishedTutorialList.Contains(tutorial.Requirements[i]))
                {
                    Logger.LogFormat("Tutorial with id ({0}) must be completed first.", tutorial.Requirements[i]);
                    return false;
                }
            }
        }

        tutorialCamera.gameObject.SetActive(true);
        currentTutorial = tutorial;
        StartCoroutine(TutorialProgress());

        OnTutorialStart?.Invoke(currentTutorial.Id);
        return true;
    }

    private IEnumerator TutorialProgress()
    {
        int tutorialIndex = 0;
        isTutorialRunning = true;
        parentTutorial.SetActive(true);

        while (tutorialIndex < currentTutorial.Steps.Length)
        {
            OnTutorialStep?.Invoke(currentTutorial.Id, tutorialIndex);
            parentMonolog.SetActive(false);

            List<GameObject> tutorialObj = null;

            isReadyForNextStep = false;
            isActionFinished = false;
            currentStep = currentTutorial.Steps[tutorialIndex];

            if (currentStep.Type == TutorialType.Monolog)
            {
                parentMonolog.SetActive(true);

                if (currentStep.TextPosition == TutorialTextPosition.Top)
                {
                    parentMonolog.GetComponent<RectTransform>().anchoredPosition =
                        new Vector2(parentMonolog.GetComponent<RectTransform>().anchoredPosition.x, Mathf.Abs(parentMonolog.GetComponent<RectTransform>().anchoredPosition.y));
                }
                else
                {
                    parentMonolog.GetComponent<RectTransform>().anchoredPosition =
                        new Vector2(parentMonolog.GetComponent<RectTransform>().anchoredPosition.x, Mathf.Abs(parentMonolog.GetComponent<RectTransform>().anchoredPosition.y) * -1);
                }

                if (useTyping)
                {
                    textMonolog.text = string.Empty;
                    isTypingFinished = false;

                    for (int i = 0; i < currentStep.Text.Length; i++)
                    {
                        textMonolog.text += currentStep.Text[i];
                        yield return new WaitForEndOfFrame();

                        if (isTypingFinished)
                            break;
                    }
                }

                textMonolog.text = currentStep.Text;
                isActionFinished = true;
                isTypingFinished = true;
            }
            else
            {
                if (currentStep.HasText)
                {
                    parentMonolog.SetActive(true);

                    if (currentStep.TextPosition == TutorialTextPosition.Top)
                    {
                        parentMonolog.GetComponent<RectTransform>().anchoredPosition =
                            new Vector2(parentMonolog.GetComponent<RectTransform>().anchoredPosition.x, Mathf.Abs(parentMonolog.GetComponent<RectTransform>().anchoredPosition.y));
                    }
                    else
                    {
                        parentMonolog.GetComponent<RectTransform>().anchoredPosition =
                            new Vector2(parentMonolog.GetComponent<RectTransform>().anchoredPosition.x, Mathf.Abs(parentMonolog.GetComponent<RectTransform>().anchoredPosition.y) * -1f);
                    }

                    if (useTyping)
                    {
                        textMonolog.text = string.Empty;
                        isTypingFinished = false;

                        for (int i = 0; i < currentStep.Text.Length; i++)
                        {
                            textMonolog.text += currentStep.Text[i];
                            yield return new WaitForEndOfFrame();

                            if (isTypingFinished)
                                break;
                        }
                    }

                    textMonolog.text = currentStep.Text;
                    isTypingFinished = true;
                }

                if (currentStep.Type == TutorialType.HandPointerClick)
                {
                    TutorialObject[] allTargets = TutorialObject.FindAll(currentStep.ObjectId);
                    tutorialObj = new List<GameObject>();

                    for (int i = 0; i < allTargets.Length; i++)
                    {
                        // Clone object
                        GameObject real = allTargets[i].gameObject;
                        GameObject clone = Instantiate(real, GetParent(real));
                        clone.transform.position = real.transform.position;

                        // Show Click Pointer
                        HandPointer.ShowClickPointerAt(clone.transform, Vector3.zero, 0f, 0f);

                        // Add to list
                        tutorialObj.Add(clone);
                    }
                }
                else if (currentStep.Type == TutorialType.HandPointerDrag)
                {
                    tutorialObj = new List<GameObject>();

                    // Clone object 1
                    GameObject real1 = TutorialObject.Find(currentStep.ObjectId).gameObject;
                    GameObject clone1 = Instantiate(real1, GetParent(real1));
                    clone1.transform.position = real1.transform.position;

                    // Clone object 2
                    GameObject real2 = TutorialObject.Find(currentStep.ObjectIdEnd).gameObject;
                    GameObject clone2 = Instantiate(real2, GetParent(real2));
                    clone2.transform.position = real2.transform.position;

                    // Show Drag n Drop Pointer
                    HandPointer.ShowDragDropPointer(clone1.transform, clone2.transform, Vector3.zero, 0f, 0f);

                    // Add to list
                    tutorialObj.Add(clone1);
                    tutorialObj.Add(clone2);

                    isActionFinished = true;
                }
                else if (currentStep.Type == TutorialType.Highlight)
                {
                    TutorialObject[] allTargets = TutorialObject.FindAll(currentStep.ObjectId);
                    tutorialObj = new List<GameObject>();

                    for (int i = 0; i < allTargets.Length; i++)
                    {
                        // Clone object
                        GameObject real = allTargets[i].gameObject;
                        GameObject clone = Instantiate(real, GetParent(real));
                        clone.transform.position = real.transform.position;
                        clone.layer = 9;

                        // Add to list
                        tutorialObj.Add(clone);
                    }

                    isActionFinished = true;
                }
            }

            if (tutorialObj != null)
            {
                for (int i = 0; i < tutorialObj.Count; i++)
                {
                    TutorialObjectException[] objectExceptions = tutorialObj[i].GetComponentsInChildren<TutorialObjectException>();
                    if (objectExceptions.Length > 0)
                    {
                        for (int j = 0; j < objectExceptions.Length; j++)
                        {
                            Destroy(objectExceptions[j].gameObject);
                        }
                    }

                    // Setup layer
                    tutorialObj[i].SetLayerRecursively(9);
                }
            }

            yield return new WaitUntil(() => isReadyForNextStep);
            yield return new WaitUntil(() => isActionFinished);

            if (tutorialObj != null)
            {
                HandPointer.HideActivePointer();
                for (int i = 0; i < tutorialObj.Count; i++)
                {
                    Destroy(tutorialObj[i]);
                }
            }

            tutorialIndex++;
        }

        // Save tutorial progress
        if (!FinishedTutorialList.Contains(currentTutorial.Id))
            FinishedTutorialList.Add(currentTutorial.Id);

        if (!string.IsNullOrEmpty(currentTutorial.Next))
        {
            StartTutorial(currentTutorial.Next);
        }
        else
        {
            parentTutorial.SetActive(false);
            tutorialCamera.gameObject.SetActive(false);
            isTutorialRunning = false;
        }

        OnTutorialEnd?.Invoke(currentTutorial.Id);
    }

    public void Next()
    {
        isReadyForNextStep = true;
    }

    private Transform GetParent(GameObject obj)
    {
        if (obj.GetComponent<RectTransform>() != null)
        {
            return parentCloneUI;
        }
        else
        {
            return parentCloneNonUI;
        }
    }

    private void HandleOnTutorialObjectClicked(TutorialObject tutorialObject)
    {
        if (!isTutorialRunning)
            return;

        OnActionPerformed?.Invoke(currentTutorial.Id, tutorialObject.ObjectId);

        isActionFinished = true;
        Next();
    }

    #region Static

    public static System.Action<string> OnTutorialStart;
    public static System.Action<string, int> OnTutorialStep;
    public static System.Action<string> OnTutorialEnd;
    public static System.Action<string, string> OnActionPerformed;

    public static bool IsRunning { get { return Instance.isTutorialRunning; } }

    public static TutorialManager Instance
    {
        get
        {
            return Get<TutorialManager>();
        }
    }

    #endregion
}