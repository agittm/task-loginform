﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneMenuManager : GameSceneManager
{
    public void Logout()
    {
        StartCoroutine(LogoutProcess());
    }

    private IEnumerator LogoutProcess()
    {
        PopupManager.Instance.ShowLoading("Logging out...");

        yield return new WaitForSecondsRealtime(2f);

        PopupManager.Instance.HideLoading();
        SceneController.LoadScene("Login");
    }
}