﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneLoginManager : GameSceneManager
{
    [Header("Login")]
    [SerializeField] GameObject panelLogin;
    [SerializeField] InputField loginUsername;
    [SerializeField] InputField loginPassword;

    [Header("Register")]
    [SerializeField] GameObject panelRegister;
    [SerializeField] InputField registerUsername;
    [SerializeField] InputField registerEmail;
    [SerializeField] InputField registerPassword;
    [SerializeField] InputField registerPasswordC;

    private void OnEnable()
    {
        loginUsername.onValueChanged.AddListener(HandleOnUsernameChanged);
        loginPassword.onValueChanged.AddListener(HandleOnPasswordChanged);
    }

    private void OnDisable()
    {
        loginUsername.onValueChanged.RemoveListener(HandleOnUsernameChanged);
        loginPassword.onValueChanged.RemoveListener(HandleOnPasswordChanged);
    }

    private void Start()
    {
        ShowLogin();
    }

    public void ShowLogin()
    {
        panelRegister.SetActive(false);
        panelLogin.SetActive(true);
    }

    public void ShowRegister()
    {
        panelLogin.SetActive(false);
        panelRegister.SetActive(true);
    }

    public void Login()
    {
        bool hasInputUsername = !string.IsNullOrEmpty(loginUsername.text);
        bool hasInputPassword = !string.IsNullOrEmpty(loginPassword.text);

        if (hasInputUsername && hasInputPassword)
        {
            StartCoroutine(LoginProcess());
        }
        else
        {
            loginUsername.GetComponent<Outline>().enabled = !hasInputUsername;
            loginPassword.GetComponent<Outline>().enabled = !hasInputPassword;
        }
    }

    private IEnumerator LoginProcess()
    {
        PopupManager.Instance.ShowLoading("Logging in...");

        yield return new WaitForSecondsRealtime(2f);

        PopupManager.Instance.HideLoading();
        SceneController.LoadScene("Menu");
    }

    public void Register()
    {
        bool hasInputUsername = !string.IsNullOrEmpty(registerUsername.text);
        bool hasInputEmail = !string.IsNullOrEmpty(registerEmail.text);
        bool hasInputPassword = !string.IsNullOrEmpty(registerPassword.text);
        bool hasInputPasswordC = !string.IsNullOrEmpty(registerPasswordC.text);

        if (hasInputUsername && hasInputEmail && hasInputPassword && hasInputPasswordC)
        {
            if (registerEmail.text.IsEmailFormatted())
            {
                if (registerPassword.text == registerPasswordC.text)
                {
                    StartCoroutine(RegisterProcess());
                }
                else
                {
                    PopupManager.Instance.ShowInformation(
                        "Oops!",
                        "Kedua password harus sesuai.",
                        "Tutup",
                        () =>
                        {
                            registerPassword.GetComponent<Outline>().enabled = true;
                            registerPasswordC.GetComponent<Outline>().enabled = true;

                            Invoke("HideError", 1f);
                        });
                }
            }
            else
            {
                PopupManager.Instance.ShowInformation(
                    "Oops!",
                    "Format email salah.",
                    "Tutup",
                    () =>
                    {
                        registerEmail.GetComponent<Outline>().enabled = true;

                        Invoke("HideError", 1f);
                    });
            }
        }
        else
        {
            registerUsername.GetComponent<Outline>().enabled = !hasInputUsername;
            registerEmail.GetComponent<Outline>().enabled = !hasInputEmail;
            registerPassword.GetComponent<Outline>().enabled = !hasInputPassword;
            registerPasswordC.GetComponent<Outline>().enabled = !hasInputPasswordC;

            Invoke("HideError", 2f);
        }
    }

    private IEnumerator RegisterProcess()
    {
        PopupManager.Instance.ShowLoading("Registering...");

        yield return new WaitForSecondsRealtime(2f);

        PopupManager.Instance.HideLoading();
        PopupManager.Instance.ShowInformation(
            "Information",
            "Berhasil mendaftarkan akun.\nSilahkan masuk.",
            "Tutup",
            () =>
            {
                ShowLogin();
            });
    }

    private void HideError()
    {
        loginUsername.GetComponent<Outline>().enabled = false;
        loginPassword.GetComponent<Outline>().enabled = false;
        registerUsername.GetComponent<Outline>().enabled = false;
        registerEmail.GetComponent<Outline>().enabled = false;
        registerPassword.GetComponent<Outline>().enabled = false;
        registerPasswordC.GetComponent<Outline>().enabled = false;
    }

    private void HandleOnUsernameChanged(string value)
    {
        loginUsername.GetComponent<Outline>().enabled = string.IsNullOrEmpty(value);
    }

    private void HandleOnPasswordChanged(string value)
    {
        loginPassword.GetComponent<Outline>().enabled = string.IsNullOrEmpty(value);
    }
}