﻿using UnityEngine;
using System.Collections.Generic;

public class Manager : MonoBehaviour
{
    private void Awake()
    {
        managers.Add(this);
        DontDestroyOnLoad(gameObject);

        OnAwake();
    }

    protected virtual void OnAwake()
    {

    }

    private void OnDestroy()
    {
        managers.Remove(this);
    }

    #region Static

    private static List<Manager> managers = new List<Manager>();

    public static bool Has<T>() where T : Manager
    {
        return managers.Exists(x => x.GetComponent<T>() != null);
    }

    public static T Get<T>() where T : Manager
    {
        if (!Has<T>())
        {
            return Instantiate(Resources.Load<T>("Core/" + typeof(T).Name));
        }

        return managers.Find(x => x.GetType().Equals(typeof(T))).GetComponent<T>();
    }

    public static void Remove<T>() where T : Manager
    {
        if (Has<T>())
        {
            Destroy(managers.Find(x => x.GetType().Equals(typeof(T))).GetComponent<T>().gameObject);
        }
    }

    #endregion
}