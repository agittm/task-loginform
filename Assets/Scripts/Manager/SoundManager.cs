﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Hellmade.Sound;

public class SoundManager : Manager
{
    [SerializeField] List<SoundClip> SFXClips;
    [SerializeField] List<SoundClip> BGMClips;

    private Dictionary<string, AudioClip> clipDict = new Dictionary<string, AudioClip>();
    private Dictionary<string, int> cachedClip = new Dictionary<string, int>();
    private Dictionary<int, Action<int>> listenerClip = new Dictionary<int, Action<int>>();

    protected override void OnAwake()
    {
        base.OnAwake();
        Init();
    }

    private void OnEnable()
    {
        Audio.OnFinishedPlaying += HandleOnAudioFinishPlaying;
    }

    private void OnDisable()
    {
        Audio.OnFinishedPlaying -= HandleOnAudioFinishPlaying;
    }

    private void Init()
    {
        transform.position = Camera.main.transform.position;

        for (int i = 0; i < SFXClips.Count; i++)
        {
            clipDict.Add(SFXClips[i].clipId, SFXClips[i].clip);
        }

        for (int i = 0; i < BGMClips.Count; i++)
        {
            clipDict.Add(BGMClips[i].clipId, BGMClips[i].clip);
        }

        PrepareAudio();

        Logger.Log("Initializing sound");
    }

    private void PrepareAudio()
    {
        for (int i = 0; i < BGMClips.Count; i++)
        {
            int id = EazySoundManager.PrepareMusic(BGMClips[i].clip, EazySoundManager.GlobalMusicVolume, true, true, 1f, 1f);
            cachedClip.Add(BGMClips[i].clipId, id);
        }

        for (int i = 0; i < SFXClips.Count; i++)
        {
            int id = EazySoundManager.PrepareSound(SFXClips[i].clip, EazySoundManager.GlobalSoundsVolume);
            cachedClip.Add(SFXClips[i].clipId, id);
        }
    }

    public bool IsMuted()
    {
        return EazySoundManager.GlobalVolume == 0f;
    }

    public void SetMute(bool isMute)
    {
        EazySoundManager.GlobalVolume = isMute ? 0f : 1f;
    }

    public void ToggleMute()
    {
        SetMute(!IsMuted());
    }

    public void PlayBGM(string clipId)
    {
        if (!clipDict.ContainsKey(clipId))
        {
            Logger.LogErrorFormat("Clip not found : {0}", clipId);
            return;
        }

        if (cachedClip.ContainsKey(clipId))
        {
            if (EazySoundManager.GetMusicAudio(cachedClip[clipId]) != null)
            {
                Logger.Log("play from cached");
                EazySoundManager.GetMusicAudio(cachedClip[clipId]).Play();
            }
            else
            {
                // caching error, recall tbe function
                cachedClip.Remove(clipId);
                PlayBGM(clipId);
            }
        }
        else
        {
            Logger.Log("play new");
            int id = EazySoundManager.PlayMusic(clipDict[clipId], EazySoundManager.GlobalMusicVolume, true, true, 1f, 1f);
            cachedClip.Add(clipId, id);
        }
    }

    public void StopBGM(string clipId)
    {
        if (cachedClip.ContainsKey(clipId))
        {
            EazySoundManager.GetAudio(cachedClip[clipId]).Stop();
        }
    }

    public void PlaySFX(string clipId, Action<int> onFinished = null)
    {
        if (!clipDict.ContainsKey(clipId))
        {
            Logger.LogErrorFormat("Clip not found : {0}", clipId);
            return;
        }

        int id = EazySoundManager.PlaySound(clipDict[clipId], EazySoundManager.GlobalSoundsVolume);

        if (onFinished != null)
        {
            listenerClip.Add(id, onFinished);
        }
    }

    private void HandleOnAudioFinishPlaying(int audioId)
    {
        // has listener
        if (listenerClip.ContainsKey(audioId))
        {
            listenerClip[audioId]?.Invoke(audioId);
            listenerClip.Remove(audioId);
        }
    }

    #region Static Utility

    public static SoundManager Instance
    {
        get
        {
            return Get<SoundManager>();
        }
    }

    #endregion
}

[Serializable]
public class SoundClip
{
    public string clipId;
    public AudioClip clip;
}