﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppManager : Manager
{
    #region Unity Default

    protected override void OnAwake()
    {
        base.OnAwake();
    }

    #endregion

    #region Main App

    public bool TestMode;

    public void InitApplication(Action OnFinish = null)
    {
        DataManager.Instance.LoadData();

        OnFinish?.Invoke();
    }

    // called for every action in the game, for easy achievement reporting
    public void ReportAction(string code, float value)
    {
        
    }

    public void AddPlayerExperience(float exp)
    {
        PlayerData.Exp += exp;
        ReportAction("EXP.ADD", exp);
    }

    public void ChangePlayerCurrency(InGameCurrency currency, float val)
    {
        PlayerData.Currencies[currency] += val;
        ReportAction(string.Format("{0}.{1}", currency.ToString().ToUpper(), val > 0 ? "ADD" : "MIN"), val);
    }

    #endregion

    #region Static Utility

    public static AppManager Instance
    {
        get
        {
            return Get<AppManager>();
        }
    }

    public static PlayerDataModel PlayerData
    {
        get
        {
            if (DataManager.playerData == null)
                DataManager.Instance.LoadData();

            return DataManager.playerData;
        }
    }

    #endregion
}

public enum InGameCurrency { Coin, Gem }