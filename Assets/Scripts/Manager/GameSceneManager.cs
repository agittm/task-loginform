﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSceneManager : MonoBehaviour
{
    private void Awake()
    {
        OnAwake();
    }

    private void Start()
    {
        OnStart();

        StartCoroutine(WaitForTransition());
    }

    private void OnEnable()
    {
        // Register Listeners
    }

    private void OnDisable()
    {
        // Unregister Listeners
    }
    
    // Called after Awake is called
    protected virtual void OnAwake()
    {

    }

    // Called after Start is called
    protected virtual void OnStart()
    {

    }

    private IEnumerator WaitForTransition()
    {
        yield return new WaitForSeconds(0.5f);
        yield return new WaitUntil(() => SceneTransition.Instance.IsAnimationFinished);
        OnFinishTransition();
    }

    // Called when transition finished
    protected virtual void OnFinishTransition()
    {

    }

    // For changing scene with transition
    protected void StartTransition(string sceneName)
    {
        SceneTransition.Instance.StartTransition(sceneName);
    }
}