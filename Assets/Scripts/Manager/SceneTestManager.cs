﻿using UnityEngine;

public class SceneTestManager : MonoBehaviour
{
    private bool isLoading;

    private void Update()
    {
        if(isLoading && TapScreen())
        {
            Manager.Get<PopupManager>().HideLoading();
            isLoading = false;
        }
    }

    private bool TapScreen()
    {
        return Input.GetMouseButtonUp(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended);
    }

    public void TestTutorial()
    {
        Manager.Get<TutorialManager>().StartTutorial("tutorial-1");
    }

    public void TestPopupInformation()
    {
        Manager.Get<PopupManager>().ShowInformation("Title", "This is content", "Close");
    }

    public void TestPopupConfirmation()
    {
        Manager.Get<PopupManager>().ShowConfirmation("Title", "This is content", "Yes", "No", false);
    }

    public void TestPopupLoading()
    {
        isLoading = true;
        Manager.Get<PopupManager>().ShowLoading("Loading Test");
    }
}