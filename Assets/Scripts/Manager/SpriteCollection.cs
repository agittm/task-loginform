﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class SpriteCollection : Manager
{
    [Header("Atlas")]
    [SerializeField] List<SpriteAtlasCollectionData> atlasCollection;

    [Header("Sprite")]
    [SerializeField] List<SpriteCollectionData> spriteCollection;

    protected override void OnAwake()
    {
        base.OnAwake();
    }

    public SpriteAtlasCollectionData GetSpriteAtlasWithId(string id)
    {
        SpriteAtlasCollectionData collection = atlasCollection.Find(x => x.id == id);

        if (collection != null)
        {
            return collection;
        }
        else
        {
            Logger.LogErrorFormat("Collection not found : {0}", id);
            return null;
        }
    }

    public SpriteCollectionData GetSpriteCollectionWithId(string id)
    {
        SpriteCollectionData collection = spriteCollection.Find(x => x.id == id);

        if (collection != null)
        {
            return collection;
        }
        else
        {
            Logger.LogErrorFormat("Collection not found : {0}", id);
            return null;
        }
    }

    #region Static
    
    public static SpriteCollection Instance
    {
        get
        {
            return Get<SpriteCollection>();
        }
    }

    #endregion
}

[System.Serializable]
public class SpriteAtlasCollectionData
{
    public string id;
    public SpriteAtlas[] atlasses;

    public Sprite GetSprite(string id)
    {
        Sprite result = null;

        for (int i = 0; i < atlasses.Length; i++)
        {
            if (atlasses[i].GetSprite(id) != null)
            {
                result = atlasses[i].GetSprite(id);
                break;
            }
        }

        if (result != null)
        {
            return result;
        }
        else
        {
            Logger.LogErrorFormat("Sprite not found : {0}", id);
            return null;
        }
    }
}

[System.Serializable]
public class SpriteCollectionData
{
    public string id;
    public Sprite[] sprites;

    public Sprite GetSprite(string id)
    {
        Sprite result = sprites.First(x => x.name == id);

        if (result != null)
        {
            return result;
        }
        else
        {
            Logger.LogErrorFormat("Sprite not found : {0}", id);
            return null;
        }
    }
}