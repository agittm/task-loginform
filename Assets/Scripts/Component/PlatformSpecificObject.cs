﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpecificObject : MonoBehaviour
{
    [Header("Show This Object On")]
    [SerializeField] bool showEditor = true;
    [SerializeField] bool showAndroid = true;
    [SerializeField] bool showIOS = true;

    private void Start()
    {
#if UNITY_EDITOR
        gameObject.SetActive(showEditor);
#elif UNITY_ANDROID
        gameObject.SetActive(showAndroid);
#elif UNITY_IOS
        gameObject.SetActive(showIOS);
#endif
    }
}