﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using TMPro;

public class LocalizedContent : MonoBehaviour
{
    public enum ContentType { Text, ImageUI, ImageObject }
    public enum TextCase { Normal, Uppercase, Lowercase }
    private static Dictionary<string, string> localizedTextList;

    public ContentType type;
    [Header("Text")]
    public string code;
    public TextCase textCase;

    [Header("Image UI/Image Object")]
    public LocalizedSprite[] sprite = new LocalizedSprite[System.Enum.GetNames(typeof(DataManager.Locale)).Length];

    [Header("Optional")]
    [SerializeField] bool addTextBefore;
    [SerializeField] string textBefore;
    [SerializeField] bool addTextAfter;
    [SerializeField] string textAfter;

    public static void LoadText()
    {
        localizedTextList = new Dictionary<string, string>();
        string filePath = "Data/localization";

        string jsonString = Resources.Load<TextAsset>(filePath).text;
        JSONNode textData = JSONNode.Parse(jsonString);

        for (int i = 0; i < textData.Count; i++)
        {
            if (!localizedTextList.ContainsKey(textData[i]["id"].Value))
                localizedTextList.Add(textData[i]["id"].Value, textData[i]["locale-" + DataManager.locale].Value);
        }
    }

    public static string GetLocaleText(string code, params string[] values)
    {
        if (localizedTextList.ContainsKey(code))
            if (values.Length == 0)
                return localizedTextList[code];
            else
            {
                string result = localizedTextList[code];

                for (int i = 0; i < values.Length; i++)
                {
                    result = result.Replace("{" + i + "}", values[i]);
                }

                return result;
            }
        else
            return code;
    }
    
    private IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();

        SetContent();
    }

    public void SetContent()
    {
        switch (type)
        {
            case ContentType.Text:
                string textToShow = code;

                if (localizedTextList.ContainsKey(code))
                {
                    textToShow = localizedTextList[code];

                    if (addTextBefore && !string.IsNullOrEmpty(textBefore))
                        textToShow = textBefore + textToShow;

                    if (addTextAfter && !string.IsNullOrEmpty(textAfter))
                        textToShow = textToShow + textAfter;

                    if (textCase == TextCase.Uppercase)
                        textToShow = textToShow.ToUpper();

                    if (textCase == TextCase.Lowercase)
                        textToShow = textToShow.ToLower();

                    textToShow = textToShow.Replace("\\n", "\n");
                }

                if (GetComponent<Text>() != null)
                    GetComponent<Text>().text = textToShow;
                else if (GetComponent<TextMeshPro>() != null)
                    GetComponent<TextMeshPro>().text = textToShow;
                else if (GetComponent<TextMeshProUGUI>() != null)
                    GetComponent<TextMeshProUGUI>().text = textToShow;
                break;
            case ContentType.ImageUI:
                GetComponent<Image>().sprite = sprite[(int)DataManager.locale].sprite;
                GetComponent<RectTransform>().sizeDelta = sprite[(int)DataManager.locale].sprite.rect.size;
                break;
            case ContentType.ImageObject:
                GetComponent<SpriteRenderer>().sprite = sprite[(int)DataManager.locale].sprite;
                break;
            default:
                break;
        }
    }
}

public static class LocalizedContentExt
{
    public static string Localize(this string code, string preText=null, string afterText=null, params string[] param)
    {
        if (preText != null)
            code = code.Insert(0, preText + ".");

        if (afterText != null)
            code += "." + afterText;

        return LocalizedContent.GetLocaleText(code, param);
    }
}

[System.Serializable]
public class LocalizedSprite
{
    public DataManager.Locale locale;
    public Sprite sprite;
}