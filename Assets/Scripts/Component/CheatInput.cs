﻿using UnityEngine;

public class CheatInput : BasicButton
{
    [SerializeField] KeyCode inputKey;

    private void Start()
    {
#if !USE_CHEAT
        Destroy(this);
#endif
    }

    private void Update()
    {
        if (Input.GetKeyUp(inputKey) && inputKey != KeyCode.None)
        {
            Click();
        }
    }
}