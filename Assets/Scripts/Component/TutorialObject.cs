﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class TutorialObject : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] string objectId;
    public string ObjectId { get { return objectId; } }

    [SerializeField] Button buttonClick;

    private void Awake()
    {
        availableObjects.Add(this);
        CheckComponent();
    }

    private void OnDestroy()
    {
        availableObjects.Remove(this);
    }

    public void Init(string objectId)
    {
        this.objectId = objectId;
    }

    private void CheckComponent()
    {
        if (!TutorialManager.IsRunning)
            return;

        if (buttonClick != null)
        {
            buttonClick.onClick.AddListener(delegate { OnClicked?.Invoke(this); });
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        OnClicked?.Invoke(this);
    }

    #region Static

    public static System.Action<TutorialObject> OnClicked;
    private static List<TutorialObject> availableObjects = new List<TutorialObject>();

    public static TutorialObject[] FindAll(string id)
    {
        return availableObjects.FindAll(x => x.objectId == id).ToArray();
    }

    public static TutorialObject Find(string id)
    {
        return availableObjects.Find(x => x.objectId == id);
    }

    public static void SetActive(string id, bool isActive)
    {
        TutorialObject[] objects = availableObjects.FindAll(x => x.objectId == id).ToArray();
        for (int i = 0; i < objects.Length; i++)
        {
            objects[i].gameObject.SetActive(isActive);
        }
    }

    #endregion
}