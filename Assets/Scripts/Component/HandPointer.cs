﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandPointer : MonoBehaviour
{
    public enum HandPointerType { Click, DragDrop }

    [SerializeField] Camera camera;

    // all pointer type
    private HandPointerType pointerType;
    private float firstDelay;
    private float nextDelay;
    private Vector3 offset = Vector3.zero;
    private Animator animator;

    // drag drop pointer specific
    private Vector3 moveSource;
    private Vector3 moveTarget;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        if (pointerType == HandPointerType.Click)
            StartCoroutine(ShowClickPointer());
        else
            StartCoroutine(ShowDragDropPointer());

        transform.GetChild(0).transform.localPosition = offset;
    }

    private IEnumerator ShowClickPointer()
    {
        yield return new WaitForSeconds(firstDelay);
        while (true)
        {
            animator.SetTrigger("Click");

            yield return new WaitUntil(()=>animator.GetCurrentAnimatorStateInfo(0).IsName("Enter"));
            yield return new WaitForSeconds(nextDelay);

            animator.ResetTrigger("Click");
        }
    }

    private IEnumerator ShowDragDropPointer()
    {
        yield return new WaitForSeconds(firstDelay);
        while (true)
        {
            transform.position = moveSource;
            animator.SetTrigger("Drag");

            yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).IsName("Enter"));
            yield return new WaitForSeconds(nextDelay);

            animator.ResetTrigger("Drop");
        }
    }

    public void StartDrag()
    {
        GetComponent<MovingObject>().MoveTo(moveTarget, (GameObject obj) =>
        {
            animator.ResetTrigger("Drag");
            animator.SetTrigger("Drop");
        });
    }

    /* Static */

    private static HandPointer activePointer;
    public static HandPointer ActivePointer
    {
        get
        {
            return activePointer;
        }
    }

    public static bool IsAnyPointerActive
    {
        get
        {
            return activePointer != null;
        }
    }

    public static void HideActivePointer()
    {
        if (IsAnyPointerActive)
        {
            Destroy(activePointer.camera.gameObject);
            Destroy(activePointer.gameObject);
        }
    }

    public static void ShowClickPointerAt(Transform t, Vector3 offset, float firstDelay = 3f, float nextDelay = 5f)
    {
        HideActivePointer();

        activePointer = Instantiate(Resources.Load<GameObject>("Core/HandPointer")).GetComponent<HandPointer>();
        activePointer.firstDelay = firstDelay;
        activePointer.nextDelay = nextDelay;
        activePointer.offset = offset;
        activePointer.pointerType = HandPointerType.Click;
        activePointer.camera.transform.SetParent(null);

        if (t.root.GetComponent<Canvas>() != null)
        {
            activePointer.transform.SetParent(t, false);
        }

        activePointer.transform.position = t.position;
    }

    public static void ShowDragDropPointer(Transform source, Transform target, Vector3 offset, float firstDelay = 3f, float nextDelay = 5f)
    {
        HideActivePointer();

        activePointer = Instantiate(Resources.Load<GameObject>("Core/HandPointer")).GetComponent<HandPointer>();
        activePointer.firstDelay = firstDelay;
        activePointer.nextDelay = nextDelay;
        activePointer.offset = offset;
        activePointer.pointerType = HandPointerType.DragDrop;
        activePointer.camera.transform.SetParent(null);

        if (source.root.GetComponent<Canvas>() != null)
        {
            activePointer.transform.SetParent(source.root, false);
        }

        activePointer.moveSource = source.position;
        activePointer.moveTarget = target.position;
    }
}