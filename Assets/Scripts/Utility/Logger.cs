﻿using UnityEngine;

public class Logger
{
    public static void Log(string debug)
    {
#if UNITY_EDITOR || SHOW_DEBUG
        Debug.Log(debug);
#endif
    }

    public static void Log(string debug, Object context)
    {
#if UNITY_EDITOR || SHOW_DEBUG
        Debug.Log(debug, context);
#endif
    }

    public static void LogFormat(string debug, params object[] obj)
    {
#if UNITY_EDITOR || SHOW_DEBUG
        Debug.LogFormat(debug, obj);
#endif
    }

    public static void LogFormat(Object context, string debug, params object[] obj)
    {
#if UNITY_EDITOR || SHOW_DEBUG
        Debug.LogFormat(context, debug, obj);
#endif
    }

    public static void LogWarning(string debug)
    {
#if UNITY_EDITOR || SHOW_DEBUG
        Debug.LogWarning(debug);
#endif
    }

    public static void LogWarning(Object context, string debug)
    {
#if UNITY_EDITOR || SHOW_DEBUG
        Debug.LogWarning(debug, context);
#endif
    }

    public static void LogWarningFormat(string debug, params object[] obj)
    {
#if UNITY_EDITOR || SHOW_DEBUG
        Debug.LogWarningFormat(debug, obj);
#endif
    }

    public static void LogWarningFormat(Object context, string debug, params object[] obj)
    {
#if UNITY_EDITOR || SHOW_DEBUG
        Debug.LogWarningFormat(context, debug, obj);
#endif
    }

    public static void LogError(string debug)
    {
#if UNITY_EDITOR || SHOW_DEBUG
        Debug.LogError(debug);
#endif
    }

    public static void LogError(string debug, Object context)
    {
#if UNITY_EDITOR || SHOW_DEBUG
        Debug.LogError(debug, context);
#endif
    }

    public static void LogErrorFormat(string debug, params object[] obj)
    {
#if UNITY_EDITOR || SHOW_DEBUG
        Debug.LogErrorFormat(debug, obj);
#endif
    }

    public static void LogErrorFormat(Object context, string debug, params object[] obj)
    {
#if UNITY_EDITOR || SHOW_DEBUG
        Debug.LogErrorFormat(context, debug, obj);
#endif
    }
}