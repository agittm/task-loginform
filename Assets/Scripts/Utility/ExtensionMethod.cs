﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethod
{
    public const string matchEmailPattern =
               @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
               + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
               + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
               + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

    public static Vector2 GetCustomPivotPoint(this Sprite sprite)
    {
        float pivotX = -sprite.bounds.center.x / sprite.bounds.extents.x / 2 + 0.5f;
        float pivotY = -sprite.bounds.center.y / sprite.bounds.extents.y / 2 + 0.5f;
        Vector2 pivot = new Vector2(pivotX, pivotY);
        return pivot;
    }

    public static Color SetAlpha(this Color color, float opacity)
    {
        return new Color(color.r, color.g, color.b, opacity / 255f);
    }

    public static void RemoveAllChildren(this Transform transform, Action<GameObject> onDestroyed = null, params GameObject[] exception)
    {
        if (transform.childCount == 0)
            return;

        for (int i = transform.childCount - 1; i >= 0; i--)
        {
            if (exception.Contains(transform.GetChild(i).gameObject))
                continue;

            onDestroyed?.Invoke(transform.GetChild(i).gameObject);
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }
    }

    public static void SetLayerRecursively(this GameObject obj, int layer)
    {
        obj.layer = layer;

        foreach (Transform child in obj.transform)
        {
            child.gameObject.SetLayerRecursively(layer);
        }
    }

    public static bool IsEmailFormatted(this string email)
    {
        if (email != null)
            return System.Text.RegularExpressions.Regex.IsMatch(email, matchEmailPattern);
        else
            return false;
    }
}