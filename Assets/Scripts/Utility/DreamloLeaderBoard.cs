using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class DreamloLeaderBoard : Manager
{
	string dreamloWebserviceURL = "http://dreamlo.com/lb/";

	public bool IUpgradedAndGotSSL = false;
	public string privateCode = "";
	public string publicCode = "";

    private System.Action<bool, string> OnGetScore;
    private System.Action<bool> OnAddScore;

    ////////////////////////////////////////////////////////////////////////////////////////////////

    // A player named Carmine got a score of 100. If the same name is added twice, we use the higher score.
    // http://dreamlo.com/lb/(your super secret very long code)/add/Carmine/100

    // A player named Carmine got a score of 1000 in 90 seconds.
    // http://dreamlo.com/lb/(your super secret very long code)/add/Carmine/1000/90

    // A player named Carmine got a score of 1000 in 90 seconds and is Awesome.
    // http://dreamlo.com/lb/(your super secret very long code)/add/Carmine/1000/90/Awesome

    ////////////////////////////////////////////////////////////////////////////////////////////////

    protected override void OnAwake()
    {
        base.OnAwake();
    }

    private void Start()
	{
		if (IUpgradedAndGotSSL) {
			dreamloWebserviceURL = "https://www.dreamlo.com/lb/";
		}
		else {
#if UNITY_WEBGL || UNITY_IOS || UNITY_ANDROID
		Logger.LogWarning("dreamlo may require https for WEBGL / IOS / ANDROID builds.");
#endif
		}
	}
	
	public void AddScore(string playerName, int totalScore, System.Action<bool> onFinish)
	{
		AddScoreWithPipe(playerName, totalScore, onFinish);
	}
	
	public void AddScore(string playerName, int totalScore, int totalSeconds, System.Action<bool> onFinish)
	{
		AddScoreWithPipe(playerName, totalScore, totalSeconds, onFinish);
	}
	
	public void AddScore(string playerName, int totalScore, int totalSeconds, string shortText, System.Action<bool> onFinish)
	{
		AddScoreWithPipe(playerName, totalScore, totalSeconds, shortText, onFinish);
	}

    // This function saves a trip to the server. Adds the score and retrieves results in one trip.
    private void AddScoreWithPipe(string playerName, int totalScore, System.Action<bool> onFinish)
	{
        OnAddScore = onFinish;
		playerName = Clean(playerName);
		StartCoroutine(AddScoreRequest(dreamloWebserviceURL + privateCode + "/add-pipe/" + UnityWebRequest.EscapeURL(playerName) + "/" + totalScore.ToString()));
	}

    private void AddScoreWithPipe(string playerName, int totalScore, int totalSeconds, System.Action<bool> onFinish)
    {
        OnAddScore = onFinish;
        playerName = Clean(playerName);
		StartCoroutine(AddScoreRequest(dreamloWebserviceURL + privateCode + "/add-pipe/" + UnityWebRequest.EscapeURL(playerName) + "/" + totalScore.ToString()+ "/" + totalSeconds.ToString()));
	}

    private void AddScoreWithPipe(string playerName, int totalScore, int totalSeconds, string shortText, System.Action<bool> onFinish)
    {
        OnAddScore = onFinish;
        playerName = Clean(playerName);
		shortText = Clean(shortText);
		
		StartCoroutine(AddScoreRequest(dreamloWebserviceURL + privateCode + "/add-pipe/" + UnityWebRequest.EscapeURL(playerName) + "/" + totalScore.ToString() + "/" + totalSeconds.ToString()+ "/" + shortText));
	}
	
	public void GetScores(System.Action<bool, string> onFinish)
	{
        OnGetScore = onFinish;
		StartCoroutine(GetScoreRequest(dreamloWebserviceURL +  publicCode  + "/pipe"));
	}
	
	public void GetSingleScore(string playerName, System.Action<bool, string> onFinish)
	{
        OnGetScore = onFinish;
		StartCoroutine(GetScoreRequest(dreamloWebserviceURL +  publicCode  + "/pipe-get/" + UnityWebRequest.EscapeURL(playerName)));
    }

    private IEnumerator AddScoreRequest(string url)
    {
        // Something not working? Try copying/pasting the url into your web browser and see if it works.
        // Logger.Log(url);

        using (UnityWebRequest www = UnityWebRequest.Get(url))
        {
            yield return www.SendWebRequest();

            if (www.isHttpError || www.isNetworkError)
                OnAddScore?.Invoke(false);
            else
                OnAddScore?.Invoke(true);
        }
    }

    private IEnumerator GetScoreRequest(string url)
	{
        // Something not working? Try copying/pasting the url into your web browser and see if it works.
        // Logger.Log(url);

        using (UnityWebRequest www = UnityWebRequest.Get(url))
		{
			yield return www.SendWebRequest();

            if (www.isHttpError || www.isNetworkError)
                OnGetScore?.Invoke(false, string.Empty);
            else
                OnGetScore?.Invoke(true, www.downloadHandler.text);
		}
	}
	
	// Keep pipe and slash out of names
	
	private string Clean(string s)
	{
		s = s.Replace("/", "");
		s = s.Replace("|", "");
		return s;
		
	}

    /* Static */

    #region Static Utility

    public static DreamloLeaderBoard Instance
    {
        get
        {
            return Get<DreamloLeaderBoard>();
        }
    }

    #endregion
}

public struct DreamloScoreData
{
    public string playerName;
    public int score;
    public int seconds;
    public string shortText;
    public string dateString;
}

public static class DreamloExtension
{
    public static List<DreamloScoreData> ToDreamloLeaderboardList(this string rawData)
    {
        var rows = rawData.Split(new char[] { '\n' }, System.StringSplitOptions.RemoveEmptyEntries);
        if (rows == null) return null;

        int rowcount = rows.Length;

        if (rowcount <= 0) return null;

        List<DreamloScoreData> scoreList = new List<DreamloScoreData>();

        for (int i = 0; i < rowcount; i++)
        {
            var values = rows[i].Split(new char[] { '|' }, System.StringSplitOptions.None);

            var current = new DreamloScoreData();
            current.playerName = values[0];
            current.score = 0;
            current.seconds = 0;
            current.shortText = "";
            current.dateString = "";
            if (values.Length > 1) current.score = CheckInt(values[1]);
            if (values.Length > 2) current.seconds = CheckInt(values[2]);
            if (values.Length > 3) current.shortText = values[3];
            if (values.Length > 4) current.dateString = values[4];
            scoreList.Add(current);
        }

        return scoreList;
    }

    private static int CheckInt(string s)
    {
        int x = 0;

        int.TryParse(s, out x);
        return x;
    }
}