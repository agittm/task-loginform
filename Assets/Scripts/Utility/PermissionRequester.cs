﻿using System.Collections;
using UnityEngine;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

public class PermissionRequester : MonoBehaviour
{
    /// <summary>
    /// DONT FORGET TO ADD PERMISSION IN CUSTOM ANDROID MANIFEST!
    /// <uses-permission android:name="android.permission.CAMERA"/>
    /// <uses-permission android:name="android.permission.RECORD_AUDIO"/>
    /// <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
    /// <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>
    /// <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
    /// <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
    /// </summary>

    [Header("Request Permission")]
    [SerializeField] bool permissionCamera;
    [SerializeField] bool permissionMicrophone;
    [SerializeField] bool permissionFineLocation;
    [SerializeField] bool permissionCoarseLocation;
    [SerializeField] bool permissionReadExternalStorage;
    [SerializeField] bool permissionWriteExternalStorage;

    public static System.Action OnFinish;

    private void Awake()
    {
        instance = this;
    }

    private IEnumerator Start()
    {
#if PLATFORM_ANDROID
        while (gameObject.activeSelf)
        {
            yield return new WaitForSeconds(1f);
            Init();
        }
#endif
        yield return null;
    }

    public void Init()
    {
#if !UNITY_EDITOR
        if (!CheckPermission())
        {
            PopupManager.Instance.ShowInformation("Permission Request", "The game needs permission to continue.\nPlease tap allow to get started.", "OK",
            () =>
            {
                RequestPermission();
            });
        }
        else
        {
            OnFinish?.Invoke();
        }
#else
        OnFinish?.Invoke();
#endif
    }

    private bool CheckPermission()
    {
#if PLATFORM_ANDROID
        if (permissionCamera && !Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            return false;
        }

        if (permissionMicrophone && !Permission.HasUserAuthorizedPermission(Permission.Microphone))
        {
            return false;
        }

        if (permissionFineLocation && !Permission.HasUserAuthorizedPermission(Permission.FineLocation))
        {
            return false;
        }

        if (permissionCoarseLocation && !Permission.HasUserAuthorizedPermission(Permission.CoarseLocation))
        {
            return false;
        }

        if (permissionReadExternalStorage && !Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead))
        {
            return false;
        }

        if (permissionWriteExternalStorage && !Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
        {
            return false;
        }
#endif

        return true;
    }

    private void RequestPermission()
    {
#if PLATFORM_ANDROID
        if (permissionCamera && !Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            Logger.Log("Requesting Permission.Camera");
            Permission.RequestUserPermission(Permission.Camera);
        }

        if (permissionMicrophone && !Permission.HasUserAuthorizedPermission(Permission.Microphone))
        {
            Logger.Log("Requesting Permission.Microphone");
            Permission.RequestUserPermission(Permission.Microphone);
        }

        if (permissionFineLocation && !Permission.HasUserAuthorizedPermission(Permission.FineLocation))
        {
            Logger.Log("Requesting Permission.FineLocation");
            Permission.RequestUserPermission(Permission.FineLocation);
        }

        if (permissionCoarseLocation && !Permission.HasUserAuthorizedPermission(Permission.CoarseLocation))
        {
            Logger.Log("Requesting Permission.CoarseLocation");
            Permission.RequestUserPermission(Permission.CoarseLocation);
        }

        if (permissionReadExternalStorage && !Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead))
        {
            Logger.Log("Requesting Permission.ExternalStorageRead");
            Permission.RequestUserPermission(Permission.ExternalStorageRead);
        }

        if (permissionWriteExternalStorage && !Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
        {
            Logger.Log("Requesting Permission.ExternalStorageWrite");
            Permission.RequestUserPermission(Permission.ExternalStorageWrite);
        }
#endif
    }

    /* Static */

    private static PermissionRequester instance;
    public static PermissionRequester GetInstance()
    {
        return instance;
    }
}